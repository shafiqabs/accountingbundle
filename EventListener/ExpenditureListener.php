<?php

namespace Terminalbd\AccountingBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\AccountingBundle\Entity\Expenditure;

class ExpenditureListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Purchase" entity
        if ($entity instanceof Expenditure) {
            $datetime = new \DateTime("now");
            $lastCode = $this->getLastCode($args, $datetime, $entity);
            $entity->setCode($lastCode+1);
            $entity->setInvoice(sprintf("%s%s%s","E-",$datetime->format('my'), str_pad($entity->getCode(),5, '0', STR_PAD_LEFT)));

        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $datetime
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args, $datetime, Expenditure $entity)
    {

        $start = $datetime->format('Y-m-01 00:00:00');
        $end = $datetime->format('Y-m-t 23:59:59');
        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository('TerminalbdAccountingBundle:Expenditure')->createQueryBuilder('e');
        $qb
            ->select('MAX(e.code)')
            ->where('e.config = :config')->setParameter('config', $entity->getConfig())
            ->andWhere('e.created >= :start') ->setParameter('start', $start)
            ->andWhere('e.created <= :end')->setParameter('end', $end);
        $lastCode = $qb->getQuery()->getSingleScalarResult();
        if (empty($lastCode)) {
            return 0;
        }
        return $lastCode;
    }
}