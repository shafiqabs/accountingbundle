<?php

namespace Terminalbd\AccountingBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\AccountingBundle\Entity\AccountSales;

class SalesListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        // perhaps you only want to act on some "Sales" entity
        if ($entity instanceof AccountSales) {

            $datetime = new \DateTime("now");
            $lastCode = $this->getLastCode($args, $datetime, $entity);
            $entity->setCode($lastCode+1);
            $entity->setInvoice(sprintf("%s%s%s","S-",$datetime->format('my'), str_pad($entity->getCode(),5, '0', STR_PAD_LEFT)));

        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $datetime
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args, $datetime, AccountSales $entity)
    {
        $today_startdatetime = $datetime->format('Y-m-01 00:00:00');
        $today_enddatetime = $datetime->format('Y-m-t 23:59:59');


        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository('TerminalbdAccountingBundle:AccountSales')->createQueryBuilder('s');

        $qb
            ->select('MAX(s.code)')
            ->where('e.config = :config')->setParameter('config', $entity->getConfig())
            ->andWhere('s.updated >= :today_startdatetime')->setParameter('today_startdatetime', $today_startdatetime)
            ->andWhere('s.updated <= :today_enddatetime')->setParameter('today_enddatetime', $today_enddatetime);
        $lastCode = $qb->getQuery()->getSingleScalarResult();

        if (empty($lastCode)) {
            return 0;
        }

        return $lastCode;
    }
}