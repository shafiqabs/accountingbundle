<?php
namespace Terminalbd\AccountingBundle\Controller;

use App\Entity\Application\Accounting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\AccountingBundle\Form\BankFormType;
use Terminalbd\AccountingBundle\Form\MobileFormType;
use Terminalbd\AccountingBundle\Repository\AccountBankRepository;

/**
 * @Route("/accounting/account-mobile")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_ACCOUNT')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class AccountMobileController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="accounting_mobile")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_ACCOUNT_SETTING')")
     */
    public function index(Request $request): ? Response
    {
        $config = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $posts = $this->getDoctrine()->getRepository('TerminalbdAccountingBundle:AccountBank')->findBy(array('config' =>$config,'mode'=>"mobile" ));
        return $this->render('@TerminalbdAccounting/accountmobile/index.html.twig', [
            'entities' => $posts
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_ACCOUNT_SETTING') and is_granted('ROLE_ACCOUNT_CREATE'))")
     * @Route("/new", methods={"GET", "POST"}, name="accounting_mobile_new")
     */
    public function new(Request $request): Response
    {
        $config = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $post = new AccountBank();
        $form = $this->createForm(MobileFormType::class , $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $post->setConfig($config);
            $post->setMode("mobile");
            $name = $post->getMobile().'-'.$post->getServiceName();
            $entity->setName($name);
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('accounting_mobile_new');
            }
            return $this->redirectToRoute('accounting_mobile_new');
        }
        return $this->render('@TerminalbdAccounting/accountmobile/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="accounting_mobile_edit")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_ACCOUNT_SETTING') and is_granted('ROLE_ACCOUNT_EDIT'))")
     */
    public function edit(Request $request, AccountBank $post): Response
    {

        $form = $this->createForm(MobileFormType::class, $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $name = $post->getMobile().'-'.$post->getServiceName();
            $entity->setName($name);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('accounting_mobile_edit', ['id' => $post->getId()]);
            }
            return $this->redirectToRoute('accounting_mobile');
        }
        return $this->render('@TerminalbdAccounting/accountmobile/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a AccountBank entity.
     *
     * @Route("/{id}/delete",methods={"GET", "POST"}, name="accounting_mobile_delete")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_ACCOUNT_SETTING') and is_granted('ROLE_ACCOUNT_DELETE'))")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(AccountBank::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }
}
