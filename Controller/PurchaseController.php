<?php
namespace Terminalbd\AccountingBundle\Controller;

use App\Entity\Application\Accounting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;
use Terminalbd\AccountingBundle\Entity\AccountPurchase;
use Terminalbd\AccountingBundle\Form\BankFormType;
use Terminalbd\AccountingBundle\Form\MobileFormType;
use Terminalbd\AccountingBundle\Repository\AccountBankRepository;

/**
 * @Route("/accounting/purchase")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_ACCOUNT')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="accounting_purchase")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_ACCOUNT_PURCHASE')")
     */
    public function index(Request $request): ? Response
    {
        $config = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $posts = $this->getDoctrine()->getRepository('TerminalbdAccountingBundle:AccountBank')->findBy(array('config' =>$config,'mode'=>"bank" ));
        return $this->render('@TerminalbdAccounting/purchase/index.html.twig', [
            'entities' => $posts
        ]);
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_ACCOUNT_PURCHASE') and is_granted('ROLE_ACCOUNT_CREATE'))")
     * @Route("/new", methods={"GET", "POST"}, name="accounting_purchase_new")
     */
    public function new(Request $request): Response
    {
        $config = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $post = new AccountBank();
        $form = $this->createForm(BankFormType::class , $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $post->setConfig($config);
            $name = $post->getBank()->getName().'-'.$post->getAccountNo();
            $post->setName($name);
            $post->setMode('bank');
            $em->persist($post);
            $em->flush();
            $this->addFlash('success', 'post.created_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('accounting_purchase_new');
            }
            return $this->redirectToRoute('accounting_purchase_new');
        }
        return $this->render('@TerminalbdAccounting/purchase/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="accounting_purchase_edit")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_ACCOUNT_PURCHASE') and is_granted('ROLE_ACCOUNT_EDIT'))")
     */
    public function edit(Request $request, AccountBank $post): Response
    {

        $form = $this->createForm(BankFormType::class, $post)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $name = $post->getBank()->getName().'-'.$post->getAccountNo();
            $post->setName($name);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'post.updated_successfully');
            if ($form->get('SaveAndCreate')->isClicked()) {
                return $this->redirectToRoute('accounting_purchase_edit', ['id' => $post->getId()]);
            }
            return $this->redirectToRoute('accounting_purchase');
        }
        return $this->render('@TerminalbdAccounting/purchase/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a AccountBank entity.
     *
     * @Route("/{id}/delete",methods={"GET", "POST"}, name="accounting_purchase_delete")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_ACCOUNT_PURCHASE') and is_granted('ROLE_ACCOUNT_DELETE'))")
     */
    public function delete($id): Response
    {
        $post = $this->getDoctrine()->getRepository(AccountBank::class)->findOneBy(['id'=> $id]);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a AccountBank entity.
     *
     * @Route("/{id}/delete",methods={"GET", "POST"}, name="accounting_purchase_delete")
     * @Security("is_granted('ROLE_DOMAIN') or (is_granted('ROLE_ACCOUNT_PURCHASE') and is_granted('ROLE_ACCOUNT_APPROVED'))")
     */
    public function approve(AccountPurchase $entity)
    {
        if (!empty($entity) and $entity->getProcess() != 'approved') {
            $em = $this->getDoctrine()->getManager();
            $entity->setProcess('approved');
            $entity->setApprovedBy($this->getUser()->getName());
            if(empty($entity->getTransactionMethod()) and in_array($entity->getProcessType(),array( 'Due','Advance'))){
                $method = $this->getDoctrine()->getRepository('SettingToolBundle:TransactionMethod')->find(1);
                $entity->setTransactionMethod($method);
            }
            $config = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
            $accountConfig = $config->isAccountClose();
            if($accountConfig == 1){
                $datetime = new \DateTime("yesterday 23:30:30");
                $entity->setCreated($datetime);
                $entity->setUpdated($datetime);
            }
            $em->flush();
            $accountPurchase = $em->getRepository('AccountingBundle:AccountPurchase')->updateVendorBalance($entity);
            if(in_array($entity->getProcessType(),array('Outstanding','Opening'))){
                $this->getDoctrine()->getRepository('TerminalbdAccountingBundle:Transaction')-> insertVendorOpeningTransaction($entity);
            }elseif($entity->getProcessType() == 'Discount'){
                $this->getDoctrine()->getRepository('TerminalbdAccountingBundle:Transaction')->insertVendorDiscountTransaction($entity);
            }elseif($entity->getPayment() > 0 ){
                $this->getDoctrine()->getRepository('TerminalbdAccountingBundle:AccountCash')->insertPurchaseCash($accountPurchase);
                $this->getDoctrine()->getRepository('TerminalbdAccountingBundle:Transaction')->insertPurchaseVendorTransaction($accountPurchase);
            }
            return new Response('success');
        }else{
            return new Response('failed');
        }

    }

    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="accounting_purchase_data_table")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_ACCOUNT_PURCHASE')")
     */

    public function dataTable(Request $request)
    {

        $query = $_REQUEST;
        $config = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $iTotalRecords = $this->getDoctrine()->getRepository(AccountPurchase::class)->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $this->getDoctrine()->getRepository('TerminalbdAccountingBundle:AccountPurchase')->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post AccountPurchase */

        foreach ($result as $post):

            $deleteUrl = $this->generateUrl('accounting_purchase_delete', array('id' => $post['id']));
            $processUrl = $this->generateUrl('accounting_purchase_process', array('id' => $post['id']));
            $checkBox = "";
            if ($post['process'] != "approved") {
                $checkBox = "<input class='process' type='checkbox' name='process' id='process' value='{$post['id']}' /> ";
            }
            $action = '';
            if ($post['process'] == "created"){
                $action = "<a  data-action='{$processUrl}' class='btn approve indigo-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-check'></i>Check</a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }elseif ($post['process'] == "checked"){
                $action = "<a  data-action='{$processUrl}' class='btn approve green-bg white-font btn-mini' data-id='{$post['id']}' href='javascript:'><i class='feather icon-check-square'></i>Approve</a>
 <a  data-action='{$deleteUrl}' class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:'><i class='fa fa-remove'></i></a>";
            }
            $created = $post['created']->format('d-m-Y');
            $records["data"][] = array(

                $id                 = $i,
                $created            = $created,
                $invoice            = $post['invoice'],
                $companyName        = $post['companyName'],
                $purchaseInvoice    = $post['purchaseInvoice'],
                $method             = $post['method'],
                $process            = ucfirst($post['process']),
                $netTotal           = $post['netTotal'],
                $payment            = $post['amount'],
                $balance            = $post['balance'],
                $action             ="<div class='btn-group'>{$action}</div>");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

}
