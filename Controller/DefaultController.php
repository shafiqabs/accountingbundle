<?php
namespace Terminalbd\AccountingBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{


    /**
     * @Route("nbrvat/home", name="vat_index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function index() {

        exit;
        return $this->render('@TerminalbdNbrvatBundle/post/index.html.twig');
    }

}