<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\AccountingBundle\Controller;

use App\Entity\Application\Accounting;
use App\Service\ConfigureManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Form\ConfigFormType;


/**
 * @Route("/accounting/config")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_ACCOUNT')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ConfigController extends AbstractController
{

    /**
     * @Route("/", methods={"GET", "POST"}, name="accounting_config")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_ACCOUNT_SETTING')")
     */
    public function edit(Request $request): Response
    {

        /* @var $entity Accounting */

        $entity = $this->getDoctrine()->getRepository(Accounting::class)->findConfig($this->getUser());
        $form = $this->createForm(ConfigFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }
        return $this->render('@TerminalbdAccounting/config.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

}
