<?php

namespace Terminalbd\AccountingBundle\Entity;

use Appstore\Bundle\DomainUserBundle\Entity\Branches;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Transaction
 *
 * @ORM\Table("acc_transaction")
 * @ORM\Entity(repositoryClass="Terminalbd\AccountingBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Accounting")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;

    
    /**
     * @var string
     *
     * @ORM\Column(name="processHead", type="string", length=50 , nullable = true)
     */
    private $processHead;

     /**
     * @var string
     *
     * @ORM\Column(name="process", type="string", length = 50 , nullable = true)
     */
    private $process;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\AccountingBundle\Entity\AccountHead", inversedBy="transactions" )
     **/
     private $accountHead;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\AccountingBundle\Entity\AccountProfit", inversedBy="transactions" )
     **/
     private $accountProfit;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\AccountingBundle\Entity\AccountHead", inversedBy="subTransactions" )
     **/
     private $subAccountHead;

 
    /**
     * @var string
     *
     * @ORM\Column(name="toIncrease", type="string", length=50 , nullable = true)
     */
    private $toIncrease;


    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float" , nullable = true)
     */
    private $amount = 0 ;

    /**
     * @var float
     *
     * @ORM\Column(name="debit", type="float" , nullable = true)
     */
    private $debit = 0 ;

    /**
     * @var float
     *
     * @ORM\Column(name="credit", type="float" , nullable = true)
     */
    private $credit = 0 ;

    /**
     * @var float
     *
     * @ORM\Column(name="balance", type="decimal" , nullable = true)
     */
    private $balance = 0 ;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text" , nullable = true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $accountRefNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $sourceInvoice;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    public function setProcess($process)
    {
        $this->process = $process;

        return $this;
    }

    /**
     * Get process
     *
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }



    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Transaction
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Transaction
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }



    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }



    /**
     * @return float
     */
    public function getDebit()
    {
        return $this->debit;
    }

    /**
     * @param float $debit
     */
    public function setDebit($debit)
    {
        $this->debit = $debit;
    }

    /**
     * @return float
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param float $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    }

    /**
     * @return string
     * Assets
     * Dividend
     * Liability
     * Owner's Equity
     * Operating Revenue
     * Operating Expense
     * Non-Operating Revenues and Expenses, Gains, and Losses
     * Marketing Expenses
     * Other
     */


    /**
     * @return AccountHead
     */
    public function getAccountHead()
    {
        return $this->accountHead;
    }

    /**
     * @param AccountHead $accountHead
     */
    public function setAccountHead($accountHead)
    {
        $this->accountHead = $accountHead;
    }

    /**
     * @return string
     */
    public function getToIncrease()
    {
        return $this->toIncrease;
    }

    /**
     * @param string $toIncrease
     */
    public function setToIncrease($toIncrease)
    {
        $this->toIncrease = $toIncrease;
    }

    /**
     * @return mixed
     */
    public function getGlobalOption()
    {
        return $this->globalOption;
    }

    /**
     * @param mixed $globalOption
     */
    public function setGlobalOption($globalOption)
    {
        $this->globalOption = $globalOption;
    }


    /**
     * @return string
     */
    public function getAccountRefNo()
    {
        return $this->accountRefNo;
    }

    /**
     * @param string $accountRefNo
     */
    public function setAccountRefNo($accountRefNo)
    {
        $this->accountRefNo = $accountRefNo;
    }

    /**
      * Set processHead
     * @param string $processHead
     * Purchase
     * Purchase Return
     * Sales
     * Sales Return
     * Journal
     * Bank
     * Expenditure
     * Payroll
     * Petty Cash
     * @return Transaction
     */


    public function getProcessHead()
    {
        return $this->processHead;
    }

    /**
     * @param string $processHead
     */
    public function setProcessHead($processHead)
    {
        $this->processHead = $processHead;
    }

    /**
     * @return Branches
     */
    public function getBranches()
    {
        return $this->branches;
    }

    /**
     * @param Branches $branches
     */
    public function setBranches($branches)
    {
        $this->branches = $branches;
    }

    /**
     * @return AccountHead
     */
    public function getSubAccountHead()
    {
        return $this->subAccountHead;
    }

    /**
     * @param AccountHead $subAccountHead
     */
    public function setSubAccountHead($subAccountHead)
    {
        $this->subAccountHead = $subAccountHead;
    }

    /**
     * @return AccountProfit
     */
    public function getAccountProfit()
    {
        return $this->accountProfit;
    }

    /**
     * @param AccountProfit $accountProfit
     */
    public function setAccountProfit($accountProfit)
    {
        $this->accountProfit = $accountProfit;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getSourceInvoice(): string
    {
        return $this->sourceInvoice;
    }

    /**
     * @param string $sourceInvoice
     */
    public function setSourceInvoice(string $sourceInvoice)
    {
        $this->sourceInvoice = $sourceInvoice;
    }




}

