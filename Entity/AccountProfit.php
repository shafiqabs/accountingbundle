<?php

namespace Terminalbd\AccountingBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Setting\Bundle\ToolBundle\Entity\GlobalOption;


/**
 * AccountBank
 *
 *
 * @ORM\Table(name="acc_profit")
 * @ORM\Entity(repositoryClass="Terminalbd\AccountingBundle\Repository\AccountProfitRepository")
 */
class AccountProfit
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Accounting")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private $config;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var \DateTime
     * @ORM\Column(name="generateMonth", type="datetime")
     */
    private $generateMonth;


    /**
     * @var integer
     *
     * @ORM\Column(name="month", type="integer",  nullable = true)
     */
    private $month = 0;


    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer",  nullable = true)
     */
    private $year = 0;


    /**
     * @var float
     *
     * @ORM\Column(name="sales", type="float",  nullable = true)
     */
    private $sales = 0;


    /**
     * @var float
     *
     * @ORM\Column(name="purchase", type="float",  nullable = true)
     */
    private $purchase = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="expenditure", type="float",  nullable = true)
     */
    private $expenditure = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="revenue", type="float",  nullable = true)
     */
    private $revenue = 0;


    /**
     * @var float
     *
     * @ORM\Column(name="profit", type="float",  nullable = true)
     */
    private $profit = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="loss", type="float",  nullable = true)
     */
    private $loss = 0;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return GlobalOption
     */
    public function getGlobalOption()
    {
        return $this->globalOption;
    }

    /**
     * @param GlobalOption $globalOption
     */
    public function setGlobalOption($globalOption)
    {
        $this->globalOption = $globalOption;
    }



    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @return Transaction
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @param int $month
     */
    public function setMonth($month)
    {
        $this->month = $month;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return float
     */
    public function getLoss()
    {
        return $this->loss;
    }

    /**
     * @param float $loss
     */
    public function setLoss($loss)
    {
        $this->loss = $loss;
    }

    /**
     * @return float
     */
    public function getProfit()
    {
        return $this->profit;
    }

    /**
     * @param float $profit
     */
    public function setProfit($profit)
    {
        $this->profit = $profit;
    }

    /**
     * @return float
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param float $sales
     */
    public function setSales($sales)
    {
        $this->sales = $sales;
    }


    /**
     * @return float
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * @param float $purchase
     */
    public function setPurchase($purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * @return float
     */
    public function getExpenditure()
    {
        return $this->expenditure;
    }

    /**
     * @param float $expenditure
     */
    public function setExpenditure($expenditure)
    {
        $this->expenditure = $expenditure;
    }

    /**
     * @return float
     */
    public function getRevenue()
    {
        return $this->revenue;
    }

    /**
     * @param float $revenue
     */
    public function setRevenue($revenue)
    {
        $this->revenue = $revenue;
    }

    /**
     * @return \DateTime
     */
    public function getGenerateMonth()
    {
        return $this->generateMonth;
    }

    /**
     * @param \DateTime $generateMonth
     */
    public function setGenerateMonth($generateMonth)
    {
        $this->generateMonth = $generateMonth;
    }

}

