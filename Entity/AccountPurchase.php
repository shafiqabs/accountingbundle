<?php

namespace Terminalbd\AccountingBundle\Entity;

use App\Entity\Application\Accounting;
use App\Entity\Core\Vendor;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * AccountPurchase
 *
 * @ORM\Table(name="acc_purchase")
 * @ORM\Entity(repositoryClass="Terminalbd\AccountingBundle\Repository\AccountPurchaseRepository")
 */
    class AccountPurchase
    {

        /**
         * @ORM\Id
         * @ORM\Column(name="id", type="guid")
         * @ORM\GeneratedValue(strategy="UUID")
         */
        protected $id;

        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Application\Accounting")
         * @ORM\JoinColumn(onDelete="CASCADE")
         **/
        private $config;

        /**
         * @ORM\ManyToOne(targetEntity="App\Entity\Core\Vendor")
         **/
        private $vendor;

        /**
         * @var float
         *
         * @ORM\Column(name="netTotal", type="float", nullable=true)
         */
        private $netTotal = 0;


        /**
         * @var float
         *
         * @ORM\Column(name="amount", type="float", nullable=true)
         */
        private $amount = 0;


          /**
         * @var float
         *
         * @ORM\Column(name="vat", type="float", nullable=true)
         */
        private $vat = 0;


         /**
         * @var float
         *
         * @ORM\Column(name="commission", type="float", nullable=true)
         */
        private $commission = 0;

         /**
         * @var float
         *
         * @ORM\Column(name="discount", type="float", nullable=true)
         */
        private $discount = 0;

        /**
         * @var float
         *
         * @ORM\Column(name="balance", type="float", nullable=true)
         */
        private $balance = 0;


        /**
         * @var string
         *
         * @ORM\Column(name="voucherType", type="string", length = 60, nullable=true)
         */
        private $voucherType = "bill";

        /**
         * @var string
         *
         * @ORM\Column(name="companyName", type="string", length = 256, nullable=true)
         */
        private $companyName;

        /**
         * @var string
         *
         * @ORM\Column(name="invoice", type="string", length = 50, nullable=true)
         */
        private $invoice;

        /**
         * @var string
         *
         * @ORM\Column(type="string", length = 50, nullable=true)
         */
        private $transactionMethod;

        /**
         * @var string
         *
         * @ORM\Column(name="memo", type="string", length = 50, nullable=true)
         */
        private $memo;


        /**
         * @var string
         *
         * @ORM\Column(name="sourceInvoice", type="string", length=50, nullable=true)
         */
        private $sourceInvoice;

        /**
         * @var string
         *
         * @ORM\Column(name="module", type="string", length=50, nullable=true)
         */
        private $module;


        /**
         * @var integer
         *
         * @ORM\Column(name="code", type="integer",  nullable=true)
         */
        private $code;

        /**
         * @var string
         *
         * @ORM\Column(name="remark", type="text", nullable = true)
         */
        private $remark;

        /**
         * @var \DateTime
         * @Gedmo\Timestampable(on="create")
         * @ORM\Column(name="receiveDate", type="datetime")
         */
        private $receiveDate;

        /**
         * @var \DateTime
         * @Gedmo\Timestampable(on="create")
         * @ORM\Column(name="created", type="datetime")
         */
        private $created;

        /**
         * @var \DateTime
         * @ORM\Column(name="updated", type="datetime", nullable = true)
         */
        private $updated;


        /**
         * @var string
         *
         * @ORM\Column(name="processType", type="string", length=50, nullable = true)
         */
        private $processType;

        /**
         * @var string
         *
         * @ORM\Column(type="string", length=100, nullable = true)
         **/
        private  $createdBy;

        /**
         * @var string
         *
         * @ORM\Column(type="string", length=100, nullable = true)
         **/
        private  $checkedBy;

        /**
         * @var string
         *
         * @ORM\Column(type="string", length=100, nullable = true)
         **/
        private  $approvedBy;



        /**
         * Get id
         *
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }


        /**
         * @return \DateTime
         */
        public function getCreated()
        {
            return $this->created;
        }

        /**
         * @param \DateTime $created
         */
        public function setCreated($created)
        {
            $this->created = $created;
        }

        /**
         * @return \DateTime
         */
        public function getUpdated()
        {
            return $this->updated;
        }

        /**
         * @param \DateTime $updated
         */
        public function setUpdated($updated)
        {
            $this->updated = $updated;
        }


        /**
         * @return Vendor
         */
        public function getVendor()
        {
            return $this->vendor;
        }

        /**
         * @param Vendor $vendor
         */
        public function setVendor($vendor)
        {
            $this->vendor = $vendor;
        }



        /**
         * @return mixed
         */
        public function getCreatedBy()
        {
            return $this->createdBy;
        }

        /**
         * @param mixed $createdBy
         */
        public function setCreatedBy($createdBy)
        {
            $this->createdBy = $createdBy;
        }

        /**
         * @return datetime
         */
        public function getReceiveDate()
        {
            return $this->receiveDate;
        }

        /**
         * @param datetime $receiveDate
         */
        public function setReceiveDate($receiveDate)
        {
            $this->receiveDate = $receiveDate;
        }

        /**
         * @return string
         */
        public function getAccountNo()
        {
            return $this->accountNo;
        }

        /**
         * @param string $accountNo
         */
        public function setAccountNo($accountNo)
        {
            $this->accountNo = $accountNo;
        }

        /**
         * @return mixed
         */
        public function getApprovedBy()
        {
            return $this->approvedBy;
        }

        /**
         * @param mixed $approvedBy
         */
        public function setApprovedBy($approvedBy)
        {
            $this->approvedBy = $approvedBy;
        }

        /**
         * @return string
         */
        public function getProcess()
        {
            return $this->process;
        }

        /**
         * @param string $process
         */
        public function setProcess($process)
        {
            $this->process = $process;
        }


        /**
         * Purchase
         * Purchase Return
         * @return string
         */
        public function getProcessType()
        {
            return $this->processType;
        }

        /**
         * @param string $processType
         */
        public function setProcessType($processType)
        {
            $this->processType = $processType;
        }


        /**
         * @return float
         */
        public function getBalance()
        {
            return $this->balance;
        }

        /**
         * @param float $balance
         */
        public function setBalance($balance)
        {
            $this->balance = $balance;
        }


        /**
         * @return int
         */
        public function getCode()
        {
            return $this->code;
        }

        /**
         * @param int $code
         */
        public function setCode($code)
        {
            $this->code = $code;
        }


        /**
         * Set processHead
         * @param string $processHead
         * Purchase
         * Purchase Return
         * Sales
         * Sales Return
         * Journal
         * Bank
         * Expenditure
         * Payroll
         * Petty Cash
         * @return Transaction
         */


        public function getProcessHead()
        {
            return $this->processHead;
        }

        /**
         * @param string $processHead
         */
        public function setProcessHead($processHead)
        {
            $this->processHead = $processHead;
        }

        /**
         * @return string
         */
        public function getTransactionMethod()
        {
            return $this->transactionMethod;
        }

        /**
         * @param string $transactionMethod
         */
        public function setTransactionMethod($transactionMethod)
        {
            $this->transactionMethod = $transactionMethod;
        }

        /**
         * @return AccountBank
         */
        public function getAccountBank()
        {
            return $this->accountBank;
        }

        /**
         * @param AccountBank $accountBank
         */
        public function setAccountBank($accountBank)
        {
            $this->accountBank = $accountBank;
        }


        /**
         * @return string
         */
        public function getRemark()
        {
            return $this->remark;
        }

        /**
         * @param string $remark
         */
        public function setRemark($remark)
        {
            $this->remark = $remark;
        }

        /**
         * @return AccountCash
         */
        public function getAccountCash()
        {
            return $this->accountCash;
        }

        /**
         * @return mixed
         */
        public function getAccountMobileBank()
        {
            return $this->accountMobileBank;
        }

        /**
         * @param mixed $accountMobileBank
         */
        public function setAccountMobileBank($accountMobileBank)
        {
            $this->accountMobileBank = $accountMobileBank;
        }

        /**
         * @return string
         */
        public function getSourceInvoice()
        {
            return $this->sourceInvoice;
        }

        /**
         * @param string $sourceInvoice
         */
        public function setSourceInvoice($sourceInvoice)
        {
            $this->sourceInvoice = $sourceInvoice;
        }



	    /**
	     * @return string
	     */
	    public function getCompanyName(){
		    return $this->companyName;
	    }

	    /**
	     * @param string $companyName
	     */
	    public function setCompanyName( string $companyName ) {
		    $this->companyName = $companyName;
	    }

        /**
         * @return string
         */
        public function getVoucherType()
        {
            return $this->voucherType;
        }

        /**
         * @param string $voucherType
         */
        public function setVoucherType($voucherType)
        {
            $this->voucherType = $voucherType;
        }

        /**
         * @return VoucherItem
         */
        public function getVoucherItems()
        {
            return $this->voucherItems;
        }

        /**
         * @return string
         */
        public function getMemo()
        {
            return $this->memo;
        }

        /**
         * @param string $memo
         */
        public function setMemo($memo)
        {
            $this->memo = $memo;
        }

        /**
         * @return float
         */
        public function getCommission()
        {
            return $this->commission;
        }

        /**
         * @param float $commission
         */
        public function setCommission($commission)
        {
            $this->commission = $commission;
        }

        /**
         * @return float
         */
        public function getNetTotal(): float
        {
            return $this->netTotal;
        }

        /**
         * @param float $netTotal
         */
        public function setNetTotal(float $netTotal)
        {
            $this->netTotal = $netTotal;
        }

        /**
         * @return float
         */
        public function getAmount(): float
        {
            return $this->amount;
        }

        /**
         * @param float $amount
         */
        public function setAmount(float $amount)
        {
            $this->amount = $amount;
        }

        /**
         * @return float
         */
        public function getDiscount(): float
        {
            return $this->discount;
        }

        /**
         * @param float $discount
         */
        public function setDiscount(float $discount)
        {
            $this->discount = $discount;
        }

        /**
         * @return string
         */
        public function getInvoice(): string
        {
            return $this->invoice;
        }

        /**
         * @param string $invoice
         */
        public function setInvoice(string $invoice)
        {
            $this->invoice = $invoice;
        }

        /**
         * @return string
         */
        public function getModule(): string
        {
            return $this->module;
        }

        /**
         * @param string $module
         */
        public function setModule(string $module)
        {
            $this->module = $module;
        }

        /**
         * @return string
         */
        public function getCheckedBy(): string
        {
            return $this->checkedBy;
        }

        /**
         * @param string $checkedBy
         */
        public function setCheckedBy(string $checkedBy)
        {
            $this->checkedBy = $checkedBy;
        }



        /**
         * @return \Appstore\Bundle\AssetsBundle\Entity\Purchase
         */
        public function getAssetsPurchase()
        {
            return $this->assetsPurchase;
        }

        /**
         * @param \Appstore\Bundle\AssetsBundle\Entity\Purchase $assetsPurchase
         */
        public function setAssetsPurchase($assetsPurchase)
        {
            $this->assetsPurchase = $assetsPurchase;
        }

        /**
         * @return Accounting
         */
        public function getConfig()
        {
            return $this->config;
        }

        /**
         * @param Accounting $config
         */
        public function setConfig($config)
        {
            $this->config = $config;
        }

        /**
         * @return float
         */
        public function getVat(): float
        {
            return $this->vat;
        }

        /**
         * @param float $vat
         */
        public function setVat(float $vat)
        {
            $this->vat = $vat;
        }



    }

