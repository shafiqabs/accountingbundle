<?php
namespace Terminalbd\AccountingBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class MobileFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('accountOwner', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
             ->add('mobile', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'mobile'],
                'required' => true
            ])
            ->add('authorised', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('serviceCharge', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('serviceName', ChoiceType::class, [
                'choices'  => ['Bkash' => 'Bkash','Rocket' => 'Rocket','Nexus' => 'Nexus','iPay' => 'iPay','Nagad' => 'Nagad','Ucash' => 'Ucash'],
                'required'    => false,
                'placeholder' => 'Service Name',
                'attr' => ['autofocus' => true,'class'=>'transaction-method'],
            ])
             ->add('accountType', ChoiceType::class, [
                'choices'  => ['Personal' => 'Personal','General' => 'General','Merchant' => 'Merchant'],
                'required'    => false,
                'placeholder' => 'Account Type',
                'attr' => ['autofocus' => true,'class'=>'transaction-method'],
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AccountBank::class,
        ]);
    }
}
