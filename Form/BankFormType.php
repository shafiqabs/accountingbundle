<?php
namespace Terminalbd\AccountingBundle\Form;

use App\Entity\Admin\Bank;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\AccountingBundle\Entity\AccountBank;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class BankFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('accountOwner', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
             ->add('accountNo', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('branch', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('accountNo', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true
            ])
            ->add('serviceCharge', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])

            ->add('address', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => false
            ])
            ->add('accountType', ChoiceType::class, [
                'choices'  => ['Savings' => 'Savings','Current' => 'Current','Other' => 'Other'],
                'required'    => true,
                'placeholder' => 'Transaction',
                'attr' => ['autofocus' => true,'class'=>'transaction-method'],
            ])
            ->add('bank', EntityType::class, [
                'class' => Bank::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a bank nam',
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AccountBank::class,
        ]);
    }
}
